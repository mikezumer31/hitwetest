package com.hitwe.tests;

import com.github.javafaker.Faker;
import com.hitwe.models.User;
import com.hitwe.models.UserPreferences;
import com.hitwe.pages.RegistrationPage;
import com.hitwe.pages.UserProfilePage;
import com.hitwe.pages.WelcomePage;
import com.hitwe.setup.Base;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import static com.codeborne.selenide.Selenide.open;
import static com.hitwe.models.UserPreferences.*;
import static org.testng.Assert.assertTrue;


public class UserTests extends Base {

    private WelcomePage welcomePage;
    private Faker faker = new Faker();

    @DataProvider
    public Object[][] users() {
        return new Object[][]{
                {new User(faker.name().firstName(), faker.name().firstName() + faker.idNumber().valid() + "@testmail.com", User.Sex.MALE, 20),
                        new UserPreferences(Sex.FEMALE, HairColor.DARK, EyeColor.LIGHT, BodyType.ORDINARY)}
        };
    }

    @Title("Register user & upload photo")
    @Test(dataProvider = "users")
    public void createUserPositiveTest(User user, UserPreferences userPreferences) {
        user.setUserPreferences(userPreferences);
        RegistrationPage registrationPage = welcomePage.choosePreferences(user);
        UserProfilePage userProfilePage = registrationPage.register(user);
        userProfilePage.uploadPhoto();
        assertTrue(userProfilePage.avatar.exists(), "Photo not uploaded");

        Assert.assertEquals(user, userProfilePage.getUser());
    }

    @BeforeClass
    public void beforeClass() {
        welcomePage = open(config.getProperty("baseUrl"), WelcomePage.class);
    }

}
