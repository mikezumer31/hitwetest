package com.hitwe.models;

import lombok.Data;

@Data
public class User {

    private String name;
    private String email;
    private Sex sex;
    private int age;
    private UserPreferences userPreferences;

    public User(String name, String email, Sex sex, int age) {
        this.name = name;
        this.email = email;
        this.sex = sex;
        this.age = age;
    }
    public User(String name, Sex sex, int age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }
    public User() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getAge() == user.getAge() &&
                getName().equals(user.getName()) &&
                getSex() == user.getSex();
    }

    public enum Sex {
        MALE,
        FEMALE
    }
}