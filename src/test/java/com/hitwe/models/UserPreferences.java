package com.hitwe.models;

import lombok.Data;

@Data
public class UserPreferences {
    private Sex sex;
    private HairColor hairColor;
    private EyeColor eyeColor;
    private BodyType bodyType;

    public UserPreferences(Sex sex, HairColor hairColor, EyeColor eyeColor, BodyType bodyType) {
        this.sex = sex;
        this.hairColor = hairColor;
        this.eyeColor = eyeColor;
        this.bodyType = bodyType;
    }

    public enum Sex {
        FEMALE,
        MALE
    }

    public enum HairColor {
        DARK,
        LIGHT
    }

    public enum EyeColor {
        DARK,
        LIGHT
    }

    public enum BodyType {
        SPORTS,
        ORDINARY
    }
}