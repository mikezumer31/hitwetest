package com.hitwe.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.google.common.io.Resources;
import com.hitwe.models.User;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static com.hitwe.setup.Base.makeScreenshot;
import static com.hitwe.setup.Utils.waitForLoading;

public class UserProfilePage {

    public SelenideElement avatar = $("div[class=\"avatar-placeholder\"][style*=\"jpg\"]");
    private SelenideElement closeAdvert = $(".interstial-close");
    private SelenideElement avatarLogo = $(".prof_add_avatar");
    private SelenideElement browseButton = $("#photo");
    private SelenideElement nameField = $("div[class=\"prof-name\"]");
    private SelenideElement genderMaleDiv = $("#gender_m");
    private SelenideElement ageDiv = $("#age");  // do refactor
    private SelenideElement acceptPhoto = $(".add-ava-go-btn");
    private SelenideElement loadingIndicator = $("[class=\"add-ava-visual-item user-avatar\"] .loading_indicator");

    private int WAIT_TIME = 2000;

    @Step("Upload picture")
    public void uploadPhoto() {
        this.closeAdvert.waitUntil(Condition.appears, WAIT_TIME).click();
        avatarLogo.waitUntil(Condition.appears, WAIT_TIME);
        avatarLogo.click();
        File file = new File(Resources.getResource("profile.png").getFile());
        waitForLoading(WAIT_TIME);
        browseButton.sendKeys(file.getAbsolutePath());
        waitForLoading(WAIT_TIME);
        this.loadingIndicator.waitUntil(Condition.disappears, WAIT_TIME * 8);
        makeScreenshot();
        acceptPhoto.click();
        System.out.println();
    }

    @Step("Get user info from page")
    public User getUser() {
        makeScreenshot();
        return new User(this.nameField.getText(),
                genderMaleDiv.isSelected() ? User.Sex.MALE : User.Sex.FEMALE,
                Integer.parseInt(ageDiv.getValue()));
    }
}