package com.hitwe.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.hitwe.models.User;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.hitwe.setup.Base.makeScreenshot;

public class RegistrationPage {

    private SelenideElement nameInput = $("input[name=\"name\"]");
    private SelenideElement emailInput = $("input[name=\"email\"]");
    private SelenideElement sexDropdown = $("select[name=\"gender\"]");
    private SelenideElement ageDropdown = $("select[name=\"age\"]");
    private SelenideElement submitBtn = $("button[type=\"submit\"]");

    @Step
    public UserProfilePage register(User user) {
        this.nameInput.waitUntil(Condition.appears, 5000).sendKeys(user.getName());
        this.emailInput.sendKeys(user.getEmail());
        if (user.getSex().equals(User.Sex.FEMALE)) {
            this.sexDropdown.selectOptionByValue("f");
        } else {
            this.sexDropdown.selectOptionByValue("m");
        }
        this.ageDropdown.selectOptionByValue(String.valueOf(user.getAge()));
        makeScreenshot();
        this.submitBtn.click();
        makeScreenshot();
        return new UserProfilePage();
    }
}