package com.hitwe.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.hitwe.models.User;
import com.hitwe.models.UserPreferences;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$$;

public class WelcomePage {

    private ElementsCollection leftButton = $$("[data-test=\"qa-left-btn\"]");
    private ElementsCollection rightButton = $$("[data-test=\"qa-right-btn\"]");

    @Step("Create user")
    public RegistrationPage choosePreferences(User user) {
        this.leftButton.first().waitUntil(Condition.appears, 1000);
        SelenideElement sexButton = user.getUserPreferences().getSex().equals(UserPreferences.Sex.FEMALE) ?
                this.leftButton.findBy(Condition.visible) : this.rightButton.findBy(Condition.visible);
        sexButton.click();

        SelenideElement hairButton = user.getUserPreferences().getHairColor().equals(UserPreferences.HairColor.LIGHT) ?
                this.rightButton.findBy(Condition.visible) : this.leftButton.findBy(Condition.visible);
        hairButton.click();

        SelenideElement eyeButton = user.getUserPreferences().getEyeColor().equals(UserPreferences.EyeColor.LIGHT) ?
                this.rightButton.findBy(Condition.visible) : this.leftButton.findBy(Condition.visible);
        eyeButton.click();

        SelenideElement bodyType = user.getUserPreferences().getBodyType().equals(UserPreferences.BodyType.ORDINARY) ?
                this.rightButton.findBy(Condition.visible) : this.leftButton.findBy(Condition.visible);
        bodyType.click();

        return new RegistrationPage();
    }
}
