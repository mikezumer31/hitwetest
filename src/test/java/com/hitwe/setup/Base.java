package com.hitwe.setup;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import ru.yandex.qatools.allure.annotations.Attachment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public abstract class Base {

    public static java.util.Properties config = null;

    public Base() {
    }

    private static void browserConfiguration() {
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.browserVersion = "68";
        Configuration.timeout = 5000;
        Configuration.collectionsTimeout = 8000;
        setChromeDriverPath();
    }

    private static void setChromeDriverPath() {
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            System.setProperty("webdriver.chrome.driver", "./geckodriver/chromedriver.exe");
        } else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            System.setProperty("webdriver.chrome.driver", "./geckodriver/chromedriver_mac");
        } else if (System.getProperty("os.name").toLowerCase().contains("linux")) {
            System.setProperty("webdriver.chrome.driver", "./geckodriver/chromedriver_linux_64");
        } else {
            try {
                throw new Exception("No os detected");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void initConfig() {
        if (config == null) {
            config = new Properties();
            String config_fileName = "project.properties";
            String config_path = System.getProperty("user.dir") + File.separator + "config" + File.separator + config_fileName;
            FileInputStream configProps = null;
            try {
                configProps = new FileInputStream(config_path);
                config.load(configProps);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] makeScreenshot() {
        return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment
    public static String attachedInfo(String string) {
        return string;
    }

    @BeforeSuite
    public void beforeSuite() {
        browserConfiguration();
        initConfig();
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) {
        if (!result.isSuccess()) {
            makeScreenshot();
        }
    }

    @AfterSuite
    public void afterSuite() {
        getWebDriver().quit();
    }
}