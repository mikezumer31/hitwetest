package com.hitwe.setup;

import lombok.extern.java.Log;

@Log
public class Utils {

    public static void waitForLoading(Integer time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
