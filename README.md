# HitWe Automation Tests

This project contains automation tests for web part of application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine. See 'Running Tests' for notes on how to run the tests.
### Prerequisites

The following things must be installed:

1. Java (1.8)
2. Maven (3.3)
3. Git
4. Google Chrome (up to Version 78.0.3904.108)

### Installing

1. Clone the project
2. Go to the root of test project


## Running the tests
Run Tests via terminal: `mvn clean install -Pfrontend`


### Getting test report

Generate and open report: `mvn allure:serve`. This will open report in browser.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Mykhailo Zasko** - *Ukraine*